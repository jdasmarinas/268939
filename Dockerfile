# syntax = docker/dockerfile:latest

FROM gitlab/gitlab-ee:latest as builder

RUN apt update && apt install -y ca-certificates rsync ruby-dev rubygems python3-pip npm && apt upgrade -y  && gem install fpm && rm -rf /var/lib/apt/lists/*

FROM sagemathinc/cocalc:latest as runtime

RUN echo "test" > /tmp/test
# vim: ft=dockerfile